//
//  SavedListDetailViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 04/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SavedListDetailViewController.h"
#import "SBJson.h"
#import "SortedShoppingListCell.h"
#import "Store.h"
#import <QuartzCore/QuartzCore.h>
#import "DisplayRouteViewController.h"

//---------------getSortedShoppingLIstForListID(get list from list id)----------
#define  GLFID_USER_ID      @"user_id"
#define  GLFID_LIST_NAME    @"list_name"
#define  GLFID_LIST_DATE    @"list_date"

#define  GLFID_PRODUCT_LIST     @"product_list"   //AN ARRAY
    #define  GLFID_PRODUCT_ID   @"product_id"
    #define  GLFID_PRODUCT_NAME @"product_name"
    #define  GLFID_STORE_ID     @"store_id"
    #define  GLFID_STORE_NAME   @"store_name"
    #define  GLFID_QUANITTY     @"quantity"
    #define  GLFID_PRICE        @"price"
    #define  GLFID_SAVING       @"saving"
    #define  GLFID_LATITUDE     @"store_latitude"
    #define  GLFID_LONGITUDE    @"store_longitude"

@interface SavedListDetailViewController ()


@end

@implementation SavedListDetailViewController
@synthesize shoppingListId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        sortedShoppingListArray = [[NSMutableArray alloc]init ];
        
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Shopping List";
    self.navigationController.navigationBar.topItem.title = @"Saved Lists";

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getLocation];
    
    dispatch_async(kBgQueue, ^{
        
        
        NSString *urlStringWithParameters = [NSString stringWithFormat:@"%@?shoppinglist_id=%@",getSortedListForIDUrl,shoppingListId];        
        NSLog(@"url:%@",urlStringWithParameters);
        
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: urlStringWithParameters] options:0 error:&error];
        glbNetworkError = error;
        [self performSelectorOnMainThread:@selector(savedListsResponseData:) 
                               withObject:data waitUntilDone:YES];
        
    });
    
    
    
}

- (void)savedListsResponseData:(NSData *)responseData {
    
    
    [activityIndicator stopAnimating];
    //parse out the json data
    
    if (glbNetworkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        
        
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *json_string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSDictionary *json = [parser objectWithString:json_string error:&error];
        
        NSLog(@"SINGLE LIST DETAIL:%@",json);
        if (error == nil)
        {

            
           NSArray *arr = [json valueForKey:GLFID_PRODUCT_LIST];
            
            for(int i=0;i<[arr count];i++)
            {
                 NSDictionary *json = [arr objectAtIndex:i];
                 Store *store = [[Store alloc]init];
                 store.storeName = [json valueForKey:GLFID_STORE_NAME];
                 store.storeProductName =[json valueForKey:GLFID_PRODUCT_NAME];
                 store.storeProductQuantity = [[json valueForKey:GLFID_QUANITTY] intValue];
                 store.storeProductPrice = [json valueForKey:GLFID_PRICE];
                 store.storeProductSaving = [json valueForKey:GLFID_SAVING];
                 store.storeId = [[json valueForKey:GLFID_STORE_ID]intValue];
                 store.storeLatitude = [json valueForKey:GLFID_LATITUDE];
                 store.storeLongitude = [json valueForKey:GLFID_LONGITUDE];
                [sortedShoppingListArray addObject:store];
                
            }
        
        

           // NSLog(@"Single List Detail Array=%@",sortedShoppingListArray);
            [shoppingListTable reloadData];
            
        }
        else {
            
            NSLog(@"Parsing problem");
            
        }
        
        
        
        
        
    }
    
}



-(IBAction)showMailPanel {
    
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            [self displayComposerSheet];
        }
        else
        {
            [self launchMailAppOnDevice];
        }
    }
    
    else
    {
        [self launchMailAppOnDevice];
    }
    
    
    
    
    
}

#pragma mark -
#pragma mark Compose Mail

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:@"Shopping List"];
    
    
    // Set up recipients
    // NSArray *toRecipients = [NSArray arrayWithObject:@"deep.arora@jagsonics.com"]; 
    // NSArray *ccRecipients = [NSArray arrayWithObjects:@"second@example.com", @"third@example.com", nil]; 
    // NSArray *bccRecipients = [NSArray arrayWithObject:@"fourth@example.com"]; 
    
    //[picker setToRecipients:toRecipients];
    // [picker setCcRecipients:ccRecipients];  
    // [picker setBccRecipients:bccRecipients];
    
    // Attach an image to the email
    /*  NSString *path = [[NSBundle mainBundle] pathForResource:@"rainy" ofType:@"png"];
     NSData *myData = [NSData dataWithContentsOfFile:path];
     [picker addAttachmentData:myData mimeType:@"image/png" fileName:@"rainy"];
     */
    // Fill out the email body text
    NSMutableString *body = (NSMutableString *)@"<table  border=\"1\"><tr style=\"font-weight:bold\"><th><font size = 1>Store</font></th><th><font size = 1>Product</font></th><th><font size = 1>Quantity</font></th><th><font size = 1>Price</font></th><th><font size = 1>Saving</font></th></tr>\n";
    
    for (int row = 0; row < [sortedShoppingListArray count]; row++) {
        Store * store = [sortedShoppingListArray objectAtIndex:row];
        
        // NSLog(@"storeName=%@ %@ ",store.storeName);
        
        body = (NSMutableString *)[body stringByAppendingFormat:[NSString stringWithFormat:@"<tr><td align=center>%@</td><td align=center>%@</td><td align=center>%d</td><td align=center>%@</td><td align=center>%@</td></tr>", store.storeName, store.storeProductName, store.storeProductQuantity, store.storeProductPrice,store.storeProductSaving]];
        
    }
    body =(NSMutableString *) [body stringByAppendingString:@"</font></table>"];
    [picker setMessageBody:body isHTML:YES];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [self presentModalViewController:picker animated:YES];
    
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{   
    
    NSString *message;
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = @"Mail canceled";
            break;
        case MFMailComposeResultSaved:
            message  = @"Mail saved";
            break;
        case MFMailComposeResultSent:
            message  = @"Mail sent";
            break;
        case MFMailComposeResultFailed:
            message = @"Delivery Failure";
            break;
        default:
            message  = @"Result: not sent";
            break;
    }
    [alertView setMessage:message];
    [alertView show];
    [self dismissModalViewControllerAnimated:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillShowNotification 
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];
}


#pragma mark -
#pragma mark Workaround

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
    /*  NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=Hello from California!";
     NSString *body = @"&body=It is raining in sunny California!";
     
     NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
     email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
     
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
     */
}


- (void)keyboardWillShow:(NSNotification *)note {
    //Get view that's controlling the keyboard
    UIWindow* keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIView* firstResponder = [keyWindow performSelector:@selector(firstResponder)];
    
    
    view = [[UIView alloc]initWithFrame:CGRectMake(-320, 155, 320, 45)];
    view.backgroundColor = [UIColor lightGrayColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(240,5,70,35);
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    
    button.backgroundColor = [UIColor darkGrayColor];
    button.layer.cornerRadius = 5;
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.layer.borderWidth = 1;
    
    
    
    [button setTitle:@"Done" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(dismissKeyboard:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [view addSubview:button];
    
    
    view.alpha = 0.0;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];  
    view.alpha = .9;
    view.frame = CGRectMake(0,155 ,320,45);
    [[[[[firstResponder superview] superview] superview] superview] addSubview:view];
    
    [UIView commitAnimations];
}

-(void)dismissKeyboard:(id)sender
{
    UIWindow* keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIView* firstResponder = [keyWindow performSelector:@selector(firstResponder)];
    [firstResponder resignFirstResponder];
    
}


- (void)keyboardWillHide:(NSNotification *)note {
    //hide button here
    [view removeFromSuperview];
    
}
-(void)getLocation
{
    
    CLController = [[CoreLocationController alloc] init];
    CLController.delegate = self;
    [CLController.locMgr startUpdatingLocation];
    
    
}

- (void)locationError:(NSError *)error
{
    if(error.code== kCLErrorDenied )
    {
        UIAlertView *noLocationAlert = [[UIAlertView alloc]initWithTitle:@"Enable GPS" message:@"1) Go to Settings \n 2) Core Locations\n3) Indian Grocery\n     4)Turn the switch on" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //  noLocationAlert.tag =  LOCATION_ALERT_TAG;
        [noLocationAlert show];
        
        
    }
    
}
- (void)locationUpdate:(CLLocation *)location
{
    [CLController.locMgr stopUpdatingLocation];
    
    latitude = location.coordinate.latitude;
    longitude = location.coordinate.longitude;
    currentLocation = [[CLLocation alloc] initWithLatitude:latitude  longitude:longitude];
    
    
}



#pragma mark - IBActions

-(IBAction)mapButtonClicked:(id)sender
{
    
    // add my current location to list
    NSMutableArray *uniqueStoresLocations = [[NSMutableArray alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    [uniqueStoresLocations addObject:loc];    //adding my location
    
    
    // fetching unique stores
    
    NSLog(@"sortedShoppingList=%@",sortedShoppingListArray);
    
    NSMutableArray *uniqueStoreArray = [[NSMutableArray alloc]init ];
    for( int i =0 ; i < [sortedShoppingListArray count]; i++)
    {
        Store *store = [sortedShoppingListArray objectAtIndex:i];
        
        
        NSPredicate * myLittlePredicate = [NSPredicate predicateWithFormat:@"(storeName like %@ )", store.storeName];
        NSArray * result = [uniqueStoreArray filteredArrayUsingPredicate:myLittlePredicate];
        
        if(result && [result count] > 0)
        {
            // do not add in array
        }
        else
        {
            
            [uniqueStoreArray addObject:store];                
            
        }
        
    }
    
    NSLog(@"the value in array %@",uniqueStoreArray);
    //creating location array for unique stores
    for(Store *store in uniqueStoreArray)
    {
        CLLocationDegrees strLatitude =  [store.storeLatitude floatValue];
        CLLocationDegrees strLongitude =   [store.storeLongitude floatValue];
        
        CLLocation *loc = [[CLLocation alloc]initWithLatitude:strLatitude longitude:strLongitude];
        [uniqueStoresLocations addObject:loc];
        
        
    }
    [uniqueStoresLocations addObject:[uniqueStoresLocations objectAtIndex:0]];  // adding my loc at last index again
    
    DisplayRouteViewController *cntrl = [[DisplayRouteViewController alloc]initWithNibName:nil bundle:nil];
    cntrl.points = uniqueStoresLocations;
    [self.navigationController pushViewController:cntrl animated:YES];
    
    
}



# pragma mark - table View methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [sortedShoppingListArray count];      
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    
    static NSString *identifier = @"CellIdentifier";
    
    SortedShoppingListCell *cell = (SortedShoppingListCell *)[tableView 
                                                              dequeueReusableCellWithIdentifier: identifier];
    if (cell == nil)  
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SortedShoppingListCell" owner:self options:nil];
        
        for (id oneObject in nib)
            if ([oneObject isKindOfClass:[SortedShoppingListCell class]])
                cell = (SortedShoppingListCell *)oneObject;
    }    
    
    // NSLog(@"recent search string=%@",[recentSearchArray objectAtIndex:0]);
    
    if(indexPath.row %2 == 0)
    {
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    else {
        cell.contentView.backgroundColor = [UIColor lightGrayColor];
    }
    

    
    NSString *price = [NSString stringWithFormat:@"$%@",((Store*)[ sortedShoppingListArray objectAtIndex:indexPath.row]).storeProductPrice];
    NSString *saving = [NSString stringWithFormat:@"%@%",((Store*)[ sortedShoppingListArray objectAtIndex:indexPath.row]).storeProductSaving];
    
    NSInteger quantity = ((Store*)[ sortedShoppingListArray objectAtIndex:indexPath.row]).storeProductQuantity;
    
    cell.store.text =((Store*)[ sortedShoppingListArray objectAtIndex:indexPath.row]).storeName;
    cell.product.text = ((Store*)[ sortedShoppingListArray objectAtIndex:indexPath.row]).storeProductName;
    
    cell.quantity.text = [NSString stringWithFormat:@"%d",quantity];
    cell.price.text = price;
    cell.saving.text = saving;  
    
    return cell;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
