//
//  SavedShoppingListsViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 04/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SavedShoppingListsViewController.h"
#import "SBJson.h"
#import "SavedListDetailViewController.h"

//-------------------------------------------------------
// getSavedShoppingLists output paramters
#define GSSL_LIST_DATE @"list_date"
#define GSSL_LIST_NAME @"list_name"
#define GSSL_SHOPPING_ID @"shoppinglist_id"
#define GSSL_TOTAL_PRICE @"total_price"
#define GSSL_USER_ID @"user_id"

//-------------------------------------------------------


#define TAG_CELL_TEXTLABEL  2

@interface SavedShoppingListsViewController ()

@end

@implementation SavedShoppingListsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"Shopping Lists";
    //  self.navigationController.navigationBar.topItem.title = backBtnText;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    dispatch_async(kBgQueue, ^{
        
       
        
        NSString * user_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"userId"];
        NSLog(@"userID=%@",user_id);
 
        
        NSString *urlStringWithParameters = [NSString stringWithFormat:@"%@?user_id=%@",getSavedShoppingListsUrl,user_id];        
        NSLog(@"url:%@",urlStringWithParameters);
        
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: urlStringWithParameters] options:0 error:&error];
        glbNetworkError = error;
        [self performSelectorOnMainThread:@selector(savedListsResponseData:) 
                               withObject:data waitUntilDone:YES];
        
    });

    
    
}

- (void)savedListsResponseData:(NSData *)responseData {
    
    
    [activityIndicator stopAnimating];
    //parse out the json data
    
    if (glbNetworkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        
        
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *json_string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSArray *json = [parser objectWithString:json_string error:&error];
        
        NSLog(@"Save list response:%@",json);
        if (error == nil)
        {
            savedListsResponseArray = json;    
            NSLog(@"savedListResponseArray=%@",savedListsResponseArray);
            [shoppingListsTable reloadData];
            
        }
        else {
            
            NSLog(@"Parsing problem");
            
        }
        
        
        
        
        
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // change it later
        
    return [savedListsResponseArray count];      
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
           
 
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
      
        cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:11];
        cell.detailTextLabel.textColor= [UIColor blackColor];;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
         cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
        
    int reverseIndex = ([savedListsResponseArray count] - indexPath.row - 1);
    
    NSString *date =  [((NSDictionary*)[savedListsResponseArray objectAtIndex:reverseIndex])valueForKey:GSSL_LIST_DATE];
    NSString *totalPrice = [((NSDictionary*)[savedListsResponseArray objectAtIndex:reverseIndex])valueForKey:GSSL_TOTAL_PRICE];
    NSString *listName = [((NSDictionary*)[savedListsResponseArray objectAtIndex:reverseIndex])valueForKey:GSSL_LIST_NAME];
    
  
    
    NSString * cellText = [NSString stringWithFormat:@"%@    $%@   ",date,totalPrice];
  
    cell.textLabel.text = listName;
    cell.detailTextLabel.text = cellText;
   

       return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     int reverseIndex = ([savedListsResponseArray count] - indexPath.row - 1);
    SavedListDetailViewController *cntrl = [[SavedListDetailViewController alloc]initWithNibName:@"SavedListDetailViewcontroller" bundle:nil];
    
    NSString *shoppingID =  [((NSDictionary*)[savedListsResponseArray objectAtIndex:reverseIndex])valueForKey:GSSL_SHOPPING_ID];    
    cntrl.shoppingListId = shoppingID;
    [self.navigationController pushViewController:cntrl animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
