//
//  Superclass.m
//  IndianShopping
//
//  Created by Jagsonics on 09/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Superclass.h"
#import  <QuartzCore/QuartzCore.h>

@interface Superclass ()

@end

@implementation Superclass

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];  

}

- (void)keyboardWillShow:(NSNotification *)note {
    //Get view that's controlling the keyboard
    UIWindow* keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIView* firstResponder = [keyWindow performSelector:@selector(firstResponder)];
    
    
    view = [[UIView alloc]initWithFrame:CGRectMake(-320, 220, 320, 45)];
    view.backgroundColor = [UIColor lightGrayColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(240,5,70,35);
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    
    button.backgroundColor = [UIColor darkGrayColor];
    button.layer.cornerRadius = 5;
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.layer.borderWidth = 1;
    
    [button setTitle:@"Done" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(dismissKeyboard:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [view addSubview:button];
    
    
    view.alpha = 0.0;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];  
    view.alpha = .9;
    view.frame = CGRectMake(0,220 ,320,45);
    [[[[[firstResponder superview] superview] superview] superview] addSubview:view];
    
    [UIView commitAnimations];
}
-(void)doneButtonPressed:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    [sender resignFirstResponder];
    UIResponder* nextResponder = [textField.superview viewWithTag:(textField.tag + 1)];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
        
        // Your code for checking it.
    }
    
}

-(void)dismissKeyboard:(id)sender
{
    UIWindow* keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIView* firstResponder = [keyWindow performSelector:@selector(firstResponder)];
    [firstResponder resignFirstResponder];
    
   
    
}


- (void)keyboardWillHide:(NSNotification *)note {
    //hide button here
    [view removeFromSuperview];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillShowNotification 
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];
    
    [view setHidden:YES];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
