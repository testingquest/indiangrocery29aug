//
//  MyAccountViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 09/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAccountViewController : UIViewController
{
    IBOutlet UILabel *rewardPointsLabel;
    IBOutlet UIImageView *qrcodeImageView;
    NSError *glbNetworkError;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    NSString *imageURL;
}

@end
