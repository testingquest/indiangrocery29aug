//
//  SavedListDetailViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 04/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "NVMapViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "CoreLocationController.h"
#import <MQMapKit/MQMapKit.h>



@interface SavedListDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,UITextFieldDelegate,CLLocationManagerDelegate,CoreLocationControllerDelegate,MQRouteDelegate>
{
    NSString *shoppingListId;
    NSMutableArray *sortedShoppingListArray;          // this array is not sorted, should be sorted in this class
    UIView *view;
    NSError *glbNetworkError;
    IBOutlet UITableView *shoppingListTable;
    
    IBOutlet UIActivityIndicatorView *activityIndicator;
    CLLocationDegrees latitude;   //double type
    CLLocationDegrees longitude;
    CoreLocationController* CLController;
    CLLocation *currentLocation;
    IBOutlet UIButton *mapButton;
}

@property(nonatomic,strong) NSString *shoppingListId;

@end
