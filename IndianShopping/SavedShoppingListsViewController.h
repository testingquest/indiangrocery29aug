//
//  SavedShoppingListsViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 04/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedShoppingListsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *shoppingListsTable;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    NSArray *savedListsResponseArray;
    
    NSError *glbNetworkError;
}

@end
