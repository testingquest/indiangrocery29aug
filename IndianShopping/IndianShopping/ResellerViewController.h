//
//  ResellerViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 09/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import "Superclass.h"

@interface ResellerViewController : Superclass<ZBarReaderDelegate,UITabBarControllerDelegate>
{
    IBOutlet UITextField *userIdfield;
    IBOutlet UITextField *amountField;
    
    IBOutlet UIActivityIndicatorView *activityIndicator;
    NSError *glbNetworkError;
    
    IBOutlet UIButton *saveButton;
     
}
-(IBAction)saveBtnPressed:(id)sender;
-(IBAction)scanButtonPressed:(id)sender;
-(IBAction)returnKeyPressed:(id)sender;

@end
