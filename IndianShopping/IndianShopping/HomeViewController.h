//
//  HomeViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ZBarSDK.h"
#import <CoreLocation/CoreLocation.h>
#import "CoreLocationController.h"

//popUpTags
#define POP_PRODCT_STORE_TAG 1
#define POP_CANCEL_TAG 2


//getFeaturedProducts output parameters
#define PRODUCT_IMAGE_HVC               @"main_image" 
#define PRODUCT_ID_HVC                  @"product_id"
#define PRODUCT_NAME_HVC                @"product_name"
#define PRODUCT_DESCRIPTION_HVC         @"product_description"
#define PRODUCT_CATEGORY_NAME_HVC       @"category_name"  //for all Products only
#define PRODUCT_PRICE_HVC               @"price" // for all products only


//getFeaturedStores output paramters
#define STORE_IMAGE_HVC                 @"store_logo" 
#define STORE_ID_HVC                    @"store_id"
#define STORE_NAME_HVC                  @"store_name"
#define STORE_DESCRIPTION_HVC           @"store_description"

//tableRow
#define rowWidth 80



//Table cell Images tag
#define PRODUCT_IMAGE_TAG 1 
#define STORE_TOP_IMAGE 3
#define STORE_BOTTOM_IMAGE 5
#define TAG_FOOTER_PRODUCT 2
#define TAG_FOOTER_TOP_IMAGE 4
#define TAG_FOOTEr_BOTTOM_IMAGE 6


//toggleButton tag
#define TOGGLE_STATE_FEATURED_STORES 7
#define TOGGLE_STATE_ALL_STORES 8


//number of request happening on view did load
#define ALL_REQUESTS 2


//background request processingQueue



//URls from where data is fetched




@interface HomeViewController : UIViewController <UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate,CLLocationManagerDelegate,CoreLocationControllerDelegate,ZBarReaderDelegate>

{


    
    IBOutlet UITableView *productsTableView;
    IBOutlet UITableView *storesTableView;
    
    IBOutlet UIView *searchTextFieldBackgrouundView;
    
    IBOutlet UILabel *storeHeaderLabel;
    IBOutlet UIButton *toggleButton;
    IBOutlet UIButton *rangeButton;
    IBOutlet UIView *backgroundView;
    
    IBOutlet UIButton *scanButton;
    IBOutlet UIView *featuredProductBgView;
    IBOutlet UIView *featuredStoresBgView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    
    NSInteger selectedRow;
    NSInteger requestProcessed;
    UILabel *progressLabel;
    UIImageView *imageView;
    UISlider *mySlider;
    int sliderValue ;
    NSError *glbNetworkError;
    
    IBOutlet UIView *popUpView;
    IBOutlet UITextView *popUpName;
    IBOutlet UITextView *popUpDescription;
    IBOutlet UIImageView *popUpImage;
    IBOutlet UIButton *popUpViewPrdBtn;
    
    // collections
    NSMutableArray *allFeaturedProducts;
    NSMutableArray *allFeaturedStores;
    NSMutableArray *allStores;
    NSMutableArray *recentSearchesArray;
    
    
    //selected item
    NSInteger selectedImageTag;
    NSString *selectedImageId;
    NSString *selectedImageName;
    
    AppDelegate *delegate;
     NSMutableString *scannedText;
    
    CLLocationManager * locationManager;
    NSString *latitude;
    NSString *longitude;
    NSString *distance;
    CoreLocationController* CLController;
    
    BOOL isFirstCall;
    BOOL isNetworkAlertShown;
 
}

@property(nonatomic,strong) IBOutlet UITableView *productsTableView;
@property(nonatomic,strong) IBOutlet UITableView *storesTableView;



//@property(nonatomic,strong) UIImage *popUpImage;

-(IBAction)scanButtonPressed:(id)sender;
//-(void)imageClicked:(id)sender;
-(IBAction)popUPAction:(id)sender;

-(IBAction)toggleButtonClick:(id)sender;
-(IBAction)dismissKeyboard:(id)sender;
-(IBAction)progressAlertView:(id)sender;


- (void)showSplash;
- (void)hideSplash;

- (void)fetchedFeaturedProductsData:(NSData *)responseData;
@end
