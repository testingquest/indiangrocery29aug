//
//  ProductListViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 18/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProductListViewController.h"
#import <QuartzCore/Quartzcore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>
#import "SBJson.h"
#import "AppDelegate.h"
//#import "GMGridView.h"
//#import "AQGridViewController.h"
//#import "AQGridViewController.h"

@interface ProductListViewController ()

@end

#define X_ONE 15
#define Y_ALL 5
#define IMAGE_HEIGHT 65
#define IMAGE_WIDTH 65
#define IMAGE_ONE_TAG 1
#define IMAGE_TWO_TAG 2
#define IMAGE_THREE_TAG 3
#define IMAGE_FOUR_TAG 4

#define TAG_LABEL_ONE 5
#define TAG_LABEL_TWO 6
#define TAG_LABEL_THREE 7
#define TAG_LABEL_FOUR 8



@implementation ProductListViewController
@synthesize productPrefix;
@synthesize navButtonTitle;
@synthesize parentClass;
@synthesize barcode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      // AQGridViewController *grid = [AQGridViewController alloc];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:FALSE];
    
    [self getLocation];
    
    
    NSLog(@"Product prefix =%@",productPrefix);
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor redColor];
    //productListTable.separatorColor = [UIColor clearColor];
    
    UIImage* navBtnImage = [UIImage imageNamed:@"NavButtonBg.png"];
    CGRect frameimg = CGRectMake(0, 0, 70,30);
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:frameimg];
    leftBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    //[leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leftBtn setTitle:navButtonTitle forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:navBtnImage forState:UIControlStateNormal];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    //leftBtn setTintColor:[UIColor redColor];
    
    [leftBtn addTarget:self action:@selector(backButtonPressed:)
      forControlEvents:UIControlEventTouchUpInside];
    // [someButton setShowsTouchWhenHighlighted:YES];
    
    [self.navigationController.navigationBar setTintColor:[((AppDelegate*)[[UIApplication sharedApplication]delegate ])colorWithHexString:@"C20907" ]];    
    self.navigationItem.title = @"Products";
}
-(void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- Location
-(void)getLocation
{
    
    CLController = [[CoreLocationController alloc] init];
    CLController.delegate = self;
    [CLController.locMgr setDistanceFilter:100];
    [CLController.locMgr startUpdatingLocation];
    
    
}

- (void)locationError:(NSError *)error
{
    if(error.code== kCLErrorDenied )
    {
        UIAlertView *noLocationAlert = [[UIAlertView alloc]initWithTitle:@"Enable GPS" message:@"1) Go to Settings \n 2) Core Locations\n3) Indian Grocery\n     4)Turn the switch on" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //  noLocationAlert.tag =  LOCATION_ALERT_TAG;
        [noLocationAlert show];
        
        
    }
    
}
- (void)locationUpdate:(CLLocation *)location
{
    [CLController.locMgr stopUpdatingLocation];
    latitude = location.coordinate.latitude;
    longitude = location.coordinate.longitude;
    
    NSLog(@"%f lat  %f long",latitude,longitude);
    
     
    //[self callDefaultServicesAsynchronously];
    [self callDefaultServicesAsynchronously];
}




-(void) callDefaultServicesAsynchronously
{
    dispatch_async(kBgQueue, ^{
        
        //from user defaults
        NSString *withInDistance = [[NSUserDefaults standardUserDefaults]valueForKey:@"distance"];
       
        NSLog(@"Product List Webservice   latitude=%f  longitude=%f  distance=%@ productPrefix=%@ ",latitude,longitude,withInDistance,productPrefix);
        
        
        NSString *urlStringWithParameters;
        if (barcode.length > 1)
        {
            
            NSLog(@"Barcode=%@",barcode);
            urlStringWithParameters = [NSString stringWithFormat:@"%@?withInDistance=%@&fromLatitude=%f&fromLongitude=%f&barcode=%@",getAllProductsUrl,withInDistance,latitude,longitude,barcode];    
        }
        else {
            urlStringWithParameters = [NSString stringWithFormat:@"%@?withInDistance=%@&fromLatitude=%f&fromLongitude=%f&productPrefix=%@",getAllProductsUrl,withInDistance,latitude,longitude,productPrefix]; 
        }
        
        NSLog(@"url:%@",urlStringWithParameters);
        
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: urlStringWithParameters] options:0 error:&error];
        glbNetworkError = error;
        [self performSelectorOnMainThread:@selector(fetchedProductListData:) 
                               withObject:data waitUntilDone:YES];
    });
    
}

- (void)fetchedProductListData:(NSData *)responseData {
    
    
    [activityIndicator stopAnimating];
    //parse out the json data
    
    if (glbNetworkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *json_string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSArray *json = [parser objectWithString:json_string error:&error]; 
        
        NSLog(@"Error=%@",error);
        NSLog(@"Featured Product Response:%@",json);    
        
        if (error == nil)
        {
            NSLog(@"getProductList response=%@",json);
            productsArray = json;            
            NSLog(@"Products Array Count=%d",[productsArray count]);
            [productsTableView reloadData];   
        }
        
        
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // change it later
    NSLog(@"ProductsArray count=%d",[productsArray count]);
    int rowsCount = (([productsArray count]+3)/4); 
    NSLog(@"row count = %d",rowsCount);
    return   rowsCount;   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    UIImageView *product_one;
    UIImageView *product_two;
    UIImageView *product_three;
    UIImageView *product_four;
    
    UILabel *lableOne;
    UILabel *lableTwo;
    UILabel *lableThree;
    UILabel *lableFour;
       
   // NSLog(@"Index Path=%d",indexPath.row);
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        
        product_one = [[UIImageView alloc] initWithFrame:CGRectMake(X_ONE,Y_ALL, IMAGE_WIDTH , IMAGE_HEIGHT)];
        product_two = [[UIImageView alloc] initWithFrame:CGRectMake(X_ONE+IMAGE_WIDTH+10,Y_ALL, IMAGE_WIDTH , IMAGE_HEIGHT)];
        product_three = [[UIImageView alloc] initWithFrame:CGRectMake(X_ONE+2*IMAGE_WIDTH+2*10,Y_ALL, IMAGE_WIDTH , IMAGE_HEIGHT)];
        product_four = [[UIImageView alloc] initWithFrame:CGRectMake(X_ONE+3*IMAGE_WIDTH+3*10,Y_ALL, IMAGE_WIDTH , IMAGE_HEIGHT)];  
        
        
        lableOne = [[UILabel alloc] initWithFrame:CGRectMake(X_ONE, 45, IMAGE_WIDTH, 25)];
        
        lableTwo = [[UILabel alloc] initWithFrame:CGRectMake(X_ONE+IMAGE_WIDTH+10,45, IMAGE_WIDTH, 25)];
        
        lableThree = [[UILabel alloc] initWithFrame:CGRectMake(X_ONE+2*IMAGE_WIDTH+2*10, 45, IMAGE_WIDTH, 25)];
        
        lableFour = [[UILabel alloc] initWithFrame:CGRectMake(X_ONE+3*IMAGE_WIDTH+3*10, 45, IMAGE_WIDTH, 25)];
        
        
        
        lableOne.backgroundColor = [UIColor blackColor];
        lableOne.tag =TAG_LABEL_ONE;
        lableOne.alpha = .6;
        lableOne.textColor = [UIColor whiteColor];
        lableOne.font =  [UIFont systemFontOfSize:10.0];
        lableOne.textAlignment = UITextAlignmentCenter;
        lableOne.numberOfLines = 2;
        lableOne.lineBreakMode = UILineBreakModeWordWrap;
        
        lableTwo.backgroundColor = [UIColor blackColor];
        lableTwo.tag =TAG_LABEL_TWO;
        lableTwo.alpha = .6;
        lableTwo.textColor = [UIColor whiteColor];
        lableTwo.font =  [UIFont boldSystemFontOfSize:10];
        lableTwo.textAlignment = UITextAlignmentCenter;
        lableTwo.numberOfLines = 2;
        lableTwo.lineBreakMode = UILineBreakModeWordWrap;

        
        lableThree.backgroundColor = [UIColor blackColor];
        lableThree.tag =TAG_LABEL_THREE;
        lableThree.alpha = .6;
        lableThree.textColor = [UIColor whiteColor];
        lableThree.font =  [UIFont boldSystemFontOfSize:10];
        lableThree.textAlignment = UITextAlignmentCenter;
        lableThree.numberOfLines = 2;
        lableThree.lineBreakMode = UILineBreakModeWordWrap;

        
        lableFour.backgroundColor = [UIColor blackColor];
        lableFour.tag =TAG_LABEL_FOUR;
        lableFour.alpha = .6;
        lableFour.textColor = [UIColor whiteColor];
        lableFour.font =  [UIFont boldSystemFontOfSize:10];
        lableFour.textAlignment = UITextAlignmentCenter;
        lableFour.numberOfLines = 2;
        lableFour.lineBreakMode = UILineBreakModeWordWrap;

        
        
        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
        UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
        UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
        UITapGestureRecognizer *tapGesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
        
        //tapGesture.numberOfTapsRequired = 1;
        
        product_one.userInteractionEnabled = YES;
        product_two.userInteractionEnabled = YES;
        product_three.userInteractionEnabled = YES;
        product_four.userInteractionEnabled = YES;
        
        
        [product_one addGestureRecognizer:tapGesture1];
        [product_two addGestureRecognizer:tapGesture2];
        [product_three addGestureRecognizer:tapGesture3];
        [product_four addGestureRecognizer:tapGesture4];
        
        
        product_one.tag = IMAGE_ONE_TAG;
        product_two.tag = IMAGE_TWO_TAG;
        product_three.tag = IMAGE_THREE_TAG;
        product_four.tag = IMAGE_FOUR_TAG;
        
        
        //product_one.layer.cornerRadius = 5;
        product_one.layer.borderWidth = 1;
        product_one.layer.borderColor = [UIColor blackColor].CGColor;
        
        //  product_two.layer.cornerRadius = 5;
        product_two.layer.borderWidth = 1;
        product_two.layer.borderColor = [UIColor blackColor].CGColor;
        
        // product_three.layer.cornerRadius = 5;
        product_three.layer.borderWidth = 1;
        product_three.layer.borderColor = [UIColor blackColor].CGColor;
        
        // product_four.layer.cornerRadius = 5;
        product_four.layer.borderWidth =1;
        product_four.layer.borderColor = [UIColor blackColor].CGColor;
        
        
        product_one.contentMode = UIViewContentModeScaleToFill;
        product_two.contentMode = UIViewContentModeScaleToFill;
        product_three.contentMode = UIViewContentModeScaleToFill;
        product_four.contentMode = UIViewContentModeScaleToFill;
        
        
        [cell.contentView addSubview:product_one];
        [cell.contentView addSubview:product_two];
        [cell.contentView addSubview:product_three];
        [cell.contentView addSubview:product_four];
        
        [cell.contentView addSubview:lableOne];
        [cell.contentView addSubview:lableTwo];
        [cell.contentView addSubview:lableThree];
        [cell.contentView addSubview:lableFour];
        
               
    }
    
    else {
        
        
        product_one = (UIImageView *)[cell.contentView viewWithTag:IMAGE_ONE_TAG];
        product_two = (UIImageView *)[cell.contentView viewWithTag:IMAGE_TWO_TAG];
        product_three = (UIImageView *)[cell.contentView viewWithTag:IMAGE_THREE_TAG];
        product_four = (UIImageView *)[cell.contentView viewWithTag:IMAGE_FOUR_TAG];
        
        lableOne = (UILabel *)[cell.contentView viewWithTag:TAG_LABEL_ONE];
        lableTwo = (UILabel *)[cell.contentView viewWithTag:TAG_LABEL_TWO];
        lableThree = (UILabel *)[cell.contentView viewWithTag:TAG_LABEL_THREE];
        lableFour = (UILabel *)[cell.contentView viewWithTag:TAG_LABEL_FOUR];
        
            
    }
    
 


    NSLog(@"indexPath.row = %d",indexPath.row);
    NSLog(@"tag1=%d \n tag2=%d \n tag3=%d \n tag4=%d \n",product_one.tag,product_two.tag,product_three.tag,product_four.tag);
    
    int index1 = indexPath.row*4 + product_one.tag - 1;
    int index2 = indexPath.row*4 + product_two.tag - 1;
    int index3=  indexPath.row*4 + product_three.tag - 1;
    int index4 = indexPath.row*4 + product_four.tag - 1;
    
    
    NSLog(@"index1:%d \n index2=%d \n index3=%d \n index4=%d \n",index1,index2,index3,index4);
   // NSLog(@"productsArray=%@",productsArray);
    if (indexPath.row < (([productsArray count]+3)/4 - 1) )// until second last row, fetch all entries
    {
        // fetch all entries
        
        [product_one setHidden:NO];
        [product_two setHidden:NO];
        [product_three setHidden:NO];
        [product_four setHidden:NO];
               
        NSString *imageUrl1 = [((NSDictionary*)[productsArray objectAtIndex:(index1)]) valueForKey:PRODUCTL_IMAGE_URL];
        
        [product_one  setImageWithURL:[NSURL URLWithString: imageUrl1]                          placeholderImage:[UIImage imageNamed:@"photofram.png"]]; 
        
        
        NSString *imageUrl2 = [((NSDictionary *)[productsArray objectAtIndex:(index2)]) valueForKey:PRODUCTL_IMAGE_URL];
       
        [product_two  setImageWithURL:[NSURL URLWithString: imageUrl2]                          placeholderImage:[UIImage imageNamed:@"photofram.png"]];  
        
        
        NSString *imageUrl3 = [((NSDictionary *)[productsArray objectAtIndex:(index3)]) valueForKey:PRODUCTL_IMAGE_URL];
        
        [product_three  setImageWithURL:[NSURL URLWithString: imageUrl3]                          placeholderImage:[UIImage imageNamed:@"photofram.png"]]; 
        
        NSString *imageUrl4 = [((NSDictionary *)[productsArray objectAtIndex:(index4)]) valueForKey:PRODUCTL_IMAGE_URL];
        
        [product_four  setImageWithURL:[NSURL URLWithString: imageUrl4]                          placeholderImage:[UIImage imageNamed:@"photofram.png"]]; 
        
        lableOne.text = [((NSDictionary*)[productsArray objectAtIndex:(index1)]) valueForKey:PRODUCTL_PRODUCT_NAME];
        
        lableTwo.text = [((NSDictionary *)[productsArray objectAtIndex:(index2)]) valueForKey:PRODUCTL_PRODUCT_NAME];
        lableThree.text = [((NSDictionary *)[productsArray objectAtIndex:(index3)]) valueForKey:PRODUCTL_PRODUCT_NAME];
        
        lableFour.text = [((NSDictionary *)[productsArray objectAtIndex:(index4)]) valueForKey:PRODUCTL_PRODUCT_NAME];
    }
    else if(indexPath.row == (([productsArray count]+3)/4 - 1) )    // last row
    {
        
         [product_one setHidden:YES];
         [product_two setHidden:YES];
         [product_three setHidden:YES];
         [product_four setHidden:YES];
        
        switch ([productsArray count]%4) {
            case 0:
                // show all four
            {
                NSString *imageUrl4 = [((NSDictionary *)[productsArray objectAtIndex:(index4)]) valueForKey:PRODUCTL_IMAGE_URL];
                [product_four setHidden:NO];
                [product_four  setImageWithURL:[NSURL URLWithString: imageUrl4]                          placeholderImage:[UIImage imageNamed:@"photofram"]]; 
                 lableFour.text = [((NSDictionary *)[productsArray objectAtIndex:(index4)]) valueForKey:PRODUCTL_PRODUCT_NAME];
            }
                
                
            case 3:
                
                // show only 3
            {
                NSString *imageUrl3 = [((NSDictionary *)[productsArray objectAtIndex:(index3)]) valueForKey:PRODUCTL_IMAGE_URL];
                [product_three setHidden:NO];
                [product_three  setImageWithURL:[NSURL URLWithString: imageUrl3]                          placeholderImage:[UIImage imageNamed:@"photofram"]];
                 lableThree.text = [((NSDictionary *)[productsArray objectAtIndex:(index3)]) valueForKey:PRODUCTL_PRODUCT_NAME];
            }
                
            case 2:
                //show first two
            {
               
                NSString *imageUrl2 = [((NSDictionary *)[productsArray objectAtIndex:(index2)]) valueForKey:PRODUCTL_IMAGE_URL];
                 [product_two setHidden:NO];
                [product_two  setImageWithURL:[NSURL URLWithString: imageUrl2]                          placeholderImage:[UIImage imageNamed:@"photofram"]];  
                
                
                lableTwo.text = [((NSDictionary *)[productsArray objectAtIndex:(index2)]) valueForKey:PRODUCTL_PRODUCT_NAME];
            }
                
            case 1:
                
            {
                
             // show first 1
                
                NSString *imageUrl1 = [((NSDictionary*)[productsArray objectAtIndex:(index1)]) valueForKey:PRODUCTL_IMAGE_URL];
                [product_one setHidden:NO];
                [product_one  setImageWithURL:[NSURL URLWithString: imageUrl1]                          placeholderImage:[UIImage imageNamed:@"photofram"]]; 
                lableOne.text = [((NSDictionary*)[productsArray objectAtIndex:(index1)]) valueForKey:PRODUCTL_PRODUCT_NAME];
                //
                
            }
                
                break;
                
            default:
                break;
        }
        // add only required
    }
        
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}


- (void)imageTapped:(UITapGestureRecognizer *)gesture 

{
    UIImageView *selectedProduct = ((UIImageView *)[gesture view]);
    UITableViewCell *cell = (UITableViewCell*)[[[gesture view]superview]superview];
    NSIndexPath *indexPath =  [productsTableView indexPathForCell:cell];
    int row = indexPath.row;
    
    int arrayElementIndex = row * 4 + selectedProduct.tag - 1;
    
    
    if ((productsArray != nil) && ([productsArray count] > 0))
    {
        NSString *selectedImageId = [[productsArray objectAtIndex:arrayElementIndex]valueForKey:PRODUCTL_ID];
        
        NSLog(@"selectedImageId=%@",selectedImageId);
        
        NSInteger productId = [selectedImageId intValue];
        
        ProductDetailViewController *cntrl = [[ProductDetailViewController alloc]initWithNibName:nil bundle:nil];
        cntrl.productId = productId;
        cntrl.navButtonTitle = @"Products";
        [self.navigationController pushViewController:cntrl animated:YES];
        NSLog(@"tag=%d",selectedProduct.tag);
        
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearDisk];
    [imageCache clearMemory];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
