//
//  MailComposerViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 26/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface MailComposerViewController : UIViewController<MFMailComposeViewControllerDelegate>

@end
