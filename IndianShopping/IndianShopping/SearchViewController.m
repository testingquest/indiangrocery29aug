//
//  SearchViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchViewController.h"
#import "AppDelegate.h"
#import "ZBarSDK.h"


#define TAG_CELL_TEXTLABEL 2
@interface SearchViewController ()

@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.tabBarItem.title = @"Search";
        self.tabBarItem.image = [UIImage imageNamed:@"Search_Unselected"];        
        
    }

    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTintColor:[((AppDelegate*)[[UIApplication sharedApplication]delegate ])colorWithHexString:@"C20907" ]];
    self.navigationItem.title = @"Search";

    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"recentSearchesKey" ];    
    recentSearchArray = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSLog(@"SearchClass Recent searcheArray from user defaults=%@",recentSearchArray);
    [recentSearchTable reloadData];
    
   
}

- (void)viewDidLoad
{
    
  // AppDelegate *delega = (AppDelegate *)[[UIApplication sharedApplication].delegate];
    
    
    [super viewDidLoad];
        
    
    [searchBg setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"SearchBar_bg"]]];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // change it later
    if (recentSearchArray != nil) 
    {
        NSLog(@"RecentSearchArray=%@",recentSearchArray);
        for (int i=0;i<[recentSearchArray count];i++)
        {
            NSLog(@"Recent Search Array entry = %@",[recentSearchArray objectAtIndex:i]);
        }
        NSLog(@"Recent search Array Count=%d",[recentSearchArray count]);
        return [recentSearchArray count];
    }
        
    return 0;      
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    UILabel *customTextLabel;
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        customTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, 280, 35)];
        customTextLabel.tag = TAG_CELL_TEXTLABEL;
        customTextLabel.font = [UIFont boldSystemFontOfSize:15];
        [cell.contentView addSubview:customTextLabel];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

    }
    else {
        customTextLabel = (UILabel *)[cell.contentView viewWithTag:TAG_CELL_TEXTLABEL];
    }
   // NSLog(@"recent search string=%@",[recentSearchArray objectAtIndex:0]);
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    NSString *searchedText = (NSString *)[recentSearchArray objectAtIndex:([recentSearchArray count] -1 -indexPath.row)];
    
    
    customTextLabel.text = searchedText;
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *selectedText = [recentSearchArray objectAtIndex:([recentSearchArray count] -1 -indexPath.row)];
    
    
    [recentSearchArray removeObjectAtIndex:([recentSearchArray count] -1 -indexPath.row)];
    NSLog(@"recentSearchArraybefore=%@",recentSearchArray);
    
    [recentSearchArray addObject:selectedText];
     NSLog(@"recentSearchArrayafter=%@",recentSearchArray);
   
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:recentSearchArray];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"recentSearchesKey"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    ProductListViewController *cntrl = [[ProductListViewController alloc]initWithNibName:nil bundle:nil];
    cntrl.productPrefix = selectedText;
    cntrl.navButtonTitle = @"Search";
    [self.navigationController pushViewController:cntrl animated:YES];
}


-(IBAction)scanButtonPressed:(id)sender
{
    
    //ScanViewController *scanViewCntrl = [[ScanViewController alloc]initWithNibName:nil bundle:nil];
    // [self presentModalViewController:scanViewCntrl animated:YES];
    
    
    ZBarReaderViewController *myReader = [ZBarReaderViewController new];
    myReader.readerDelegate = self;
    myReader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = myReader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    [self presentModalViewController: myReader
                            animated: YES];
    
    //[self.navigationController pushViewController:myReader animated:YES];
    //[self.navigationController.navigationBar setHidden:YES];
    //[imageView removeFromSuperview];     // image in nav BAr
    
}


- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    //  myReader = reader;
    //myReader = reader;
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    
    
    
   NSString * scannedText = (NSMutableString *)symbol.data;
    
    ProductListViewController *cntrl = [[ProductListViewController alloc]initWithNibName:nil bundle:nil];
    cntrl.barcode = scannedText;
    cntrl.navButtonTitle = @"Search";
    [self.navigationController pushViewController:cntrl animated:YES];
    
    
    
    [self dismissModalViewControllerAnimated:YES];
    
    
    // UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Scanned Data" message:scannedText delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    //[alertView show];
}

-(IBAction)returnKeyPressed:(id)sender        
{
    [sender resignFirstResponder]; 
    UITextView *searchTextBox = (UITextView *)sender;
    
    NSString *searchText = searchTextBox.text;
    

    if (recentSearchArray == nil)
    {
        recentSearchArray = [[NSMutableArray alloc]init];
    }
    if(![recentSearchArray containsObject:searchText])
    {
        if(searchText.length > 0)
        [recentSearchArray addObject:searchText];
        
        
        if ([recentSearchArray count] > 50)
            [recentSearchArray removeObjectAtIndex:0];
        
        
    }
    else {
        [recentSearchArray removeObject:searchText];
        if(searchText.length > 0)
        [recentSearchArray addObject:searchText];
    }
    
    [recentSearchTable reloadData];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:recentSearchArray];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"recentSearchesKey"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
   /* NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"recentSearchesKey" ];    
    
    NSLog(@"Saved set in userDefault=%@",(NSMutableSet *)[NSKeyedUnarchiver unarchiveObjectWithData:data]);*/
    
    
    ProductListViewController *cntrl = [[ProductListViewController alloc]initWithNibName:nil bundle:nil];
    if (searchTextBox.text.length > 0)
    {
        cntrl.productPrefix = searchTextBox.text;
        cntrl.navButtonTitle = @"Search";
        [self.navigationController pushViewController:cntrl animated:YES];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Enter some text for search to began" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
