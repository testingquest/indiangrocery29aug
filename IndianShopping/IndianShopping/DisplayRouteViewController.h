//
//  DisplayRouteViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 11/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MQMapKit/MQMapKit.h>
#import <CoreLocation/CoreLocation.h>
#import  <MapKit/MKReverseGeocoder.h>

@interface DisplayRouteViewController : UIViewController<MQRouteDelegate,MKReverseGeocoderDelegate >
{
    NSMutableArray *points;
    MQMapView *map;
    NSMutableArray *routesArray;
    int routeLoadCount;
    NSMutableData *responseData;
    NSMutableArray *addressArray;
 
    NSMutableArray *uniqueStoresLocations;
}
@property(nonatomic,strong) NSMutableArray *points;

@end
