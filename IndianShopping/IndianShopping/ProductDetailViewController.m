//
//  ProductDetailViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 14/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProductDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>
#import <QuartzCore/QuartzCore.h>
#import "HomeViewController.h"
#import "ProductListViewController.h"
#import "SBJson.h"
#import "AppDelegate.h"
#import "FinalSortedListViewController.h"

#define TABLE_LABEL_TAG 1
#define TABLE_CHECKBOX_TAG 2


@interface ProductDetailViewController ()

@end

@implementation ProductDetailViewController
@synthesize productId;
@synthesize navButtonTitle;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        storesArray = [[NSMutableArray alloc]init];
        selectedRow = 0;
        
        
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog(@"ProductID=%d",productId);
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"shoppingListKey" ];    
    shoppingListArray = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (shoppingListArray == nil)
    {
        shoppingListArray = [[NSMutableArray alloc]initWithCapacity:1];
    }
    NSLog(@"Shopping listfrom user defaults=%@",shoppingListArray);
    
    
    
}
-(void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [addButton setEnabled:NO];
    
    [self callDefaultServicesAsynchronously];
    
    
    
    UIImage* navBtnImage = [UIImage imageNamed:@"NavButtonBg"];
    CGRect frameimg = CGRectMake(0, 0, 80, 30);
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:frameimg];
    //[leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [leftBtn setTitle:navButtonTitle forState:UIControlStateNormal];
    leftBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [leftBtn setBackgroundImage:navBtnImage forState:UIControlStateNormal];
    
    [leftBtn addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    [self.navigationController.navigationBar setTintColor:[((AppDelegate*)[[UIApplication sharedApplication]delegate ])colorWithHexString:@"C20907" ]];   
    self.navigationItem.title = @"Product";
    
    
    NSLog(@"ProductID=%d",productId);
    
    productImage.layer.borderColor = [UIColor blackColor].CGColor;
    productImage.layer.borderWidth = 2;
    productImage.layer.cornerRadius = 4;
    productImage.contentMode = UIViewContentModeScaleToFill;
    
    //
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"popMap" object:nil];     
    switch (buttonIndex) {
        case 0:
            //save
        {
            // [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isListFinalisedKey"];
            // [[NSUserDefaults standardUserDefaults]synchronize];
            self.tabBarController.selectedIndex = 2;
            
            
            
            break;
            
        case 1:
            {
                //delete{
                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"shoppingListKey"];  
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isListFinalisedKey"];
                // notify the FinalSortedList to pop to root view Controller
                // [[NSNotificationCenter defaultCenter] postNotificationName:@"StoreAddedNotification" object:nil];
                
                //clear the array
                [shoppingListArray removeAllObjects];
                for (Store *store in storesArray)
                {
                    store.storeProductQuantity = 0;
                }
                [self addEntryToList];
                self.tabBarController.selectedIndex = 2;  
            }
        default:
            break;
        }
    }
}
-(IBAction)addToListPressed:(id)sender
{
    
    //NSLog(@"%@",[[NSUserDefaults standardUserDefaults] boolForKey:@"isListFinalisedKey"]);
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isListFinalisedKey"])
    {
        // remove the data from the previous list
        
        UIAlertView *addAlert = [[UIAlertView alloc]initWithTitle:nil message:@"What do you want to do with prevoius unsaved list ? " delegate:self cancelButtonTitle:@"Save" otherButtonTitles:@"Delete", nil];
        [addAlert show];
        
        
    }
    
    else 
    {
        
        [self addEntryToList];
        self.tabBarController.selectedIndex = 2;          
    }
    
    
    // user has finalised the previous list and trying to add another item
    
    
}

-(void)addEntryToList
{
    // list of all stores where product is available
    Store *selectedStore = ((Store *)[storesArray objectAtIndex:selectedRow]);
    
    // added store
    NSInteger selectedProductId = [selectedStore storeProductId];
    NSInteger selectedStoreId = [selectedStore storeId];
    NSLog(@"Selected Product ID = %d and selected storeID = %d",selectedProductId,selectedStoreId);
    
    BOOL isAllreadyInList = NO;
    
    for (Store *store in shoppingListArray)
    {
        // NSLog(@"store id=%@")
        
        if (store.storeProductId  == selectedProductId)
        {
            
            
            if(store.storeId == selectedStoreId)
            {
                
                //allready in the list, just increase the quanitity of the product
                store.storeProductQuantity = store.storeProductQuantity + 1;
                isAllreadyInList = YES;
                break;
            }
        }
    }
    if (!isAllreadyInList)     // if not in the list. then add it
    {
        //  selectedStore.storeProductQuantity = selectedStore.storeProductQuantity + 1;
        selectedStore.storeProductQuantity = 1;
        [shoppingListArray addObject:selectedStore];
    }
    
    
    
    NSLog(@"shopping List count = %d and list is %@",[shoppingListArray count],shoppingListArray);
    
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shoppingListArray];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"shoppingListKey"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}

-(void) callDefaultServicesAsynchronously
{
    dispatch_async(kBgQueue, ^{
        
        
        NSString *urlStringWithParameters = [NSString stringWithFormat:@"%@?product_id=%d",getProductDetailUrl,productId];
        
        NSLog(@"url:%@",urlStringWithParameters);
        
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: urlStringWithParameters] options:0 error:&error];
        glbNetworkError = error;
        [self performSelectorOnMainThread:@selector(fetchedProductDetailData:) 
                               withObject:data waitUntilDone:YES];
    });
    
}

- (void)fetchedProductDetailData:(NSData *)responseData {
    
    
    [activityIndicator stopAnimating];
    //parse out the json data
    
    if (glbNetworkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *json_string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSArray *json = [parser objectWithString:json_string error:&error]; 
        
        NSLog(@"Error=%@",error);
        NSLog(@"Product Detail:%@",json);    
        productDetailDictionary = ((NSDictionary*)[json objectAtIndex:0]) ;            
        NSLog(@"ProductDetailDictionary=%@",productDetailDictionary);
        
        NSArray *arr =  (NSArray*)[productDetailDictionary valueForKey:PRODUCT_AVAILABILITY];
        for (int i=0;i<[arr count];i++)
        {
            
            NSDictionary *dict = [arr objectAtIndex:i];
            Store *store = [[Store alloc]init];
            
            store.storeAddress = [dict valueForKey:PRODUCT_STORE_ADDRESS];
            store.storeId = [[dict valueForKey:PRODUCT_STORE_ID]intValue];
            store.storeLatitude = [dict valueForKey:PRODCUT_STORE_LATITUDE];
            store.storeLongitude = [dict valueForKey:PRODUCT_STORE_LONGITUDE];
            
            store.storeProductPrice = [dict valueForKey:PRODUCT_STORE_PRICE];
            
            store.storeName = [dict valueForKey:PRODUCT_STORE_NAME];
            store.storeProductSaving = [ dict valueForKey:PRODUCT_STORE_SAVING];
            store.storeProductQuantity = 0;
            //d store.isStoreChecked = NO;
            
            store.storeProductName =[ productDetailDictionary valueForKey:PRODUCT_NAME];
            store.storeProductId = [[ productDetailDictionary valueForKey:PRODUCT_ID ]intValue]; 
            [storesArray addObject:store];
            
        }
        
        productName.text = [productDetailDictionary valueForKey:PRODUCT_NAME];
        brand.text = [productDetailDictionary valueForKey:PRODUCT_MANUFACTURER_NAME];
        //model.text = 
        priceRange.text = [productDetailDictionary valueForKey:PRODUCT_PRICE_RANGE];
        moreDetail.text = [productDetailDictionary valueForKey:PRODUCT_DESCRIPTION];
        [productImage  setImageWithURL:[NSURL URLWithString: [productDetailDictionary valueForKey:PRODUCT_IMAGE_URL]]                          placeholderImage:[UIImage imageNamed:@"photofram"]];
        
        [storesTable reloadData];
        [addButton setEnabled:YES];
        
        
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"ProductDetailDictionary=%@",productDetailDictionary);
    NSArray *prdStores = (NSArray*)[productDetailDictionary valueForKey:PRODUCT_AVAILABILITY];
    NSLog(@"PrdStores=%@",prdStores);
    NSLog(@"totalStores=%d",[prdStores count]);
    return  [prdStores count];      
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    
    
    static NSString *CellIdentifier = @"Cell";
    UILabel *label;
    UIButton *checkbox;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        label = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 220, 20)];
        checkbox = [[UIButton alloc]initWithFrame:CGRectMake(280, 10, 25, 25)];
        checkbox.tag= TABLE_CHECKBOX_TAG;
        [checkbox addTarget:self action:@selector(checkBoxClicked:) forControlEvents:UIControlEventTouchUpInside];
        [checkbox setImage:[UIImage imageNamed:@"RedioBtnselected.png"] forState:UIControlStateSelected];
        [checkbox setImage:[UIImage imageNamed:@"RedioBtnUnselected.png"] forState:UIControlStateNormal];
        label.tag = TABLE_LABEL_TAG;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:16.0];
        label.textColor = [UIColor blackColor];
        [cell.contentView addSubview:checkbox];
        [cell.contentView addSubview:label];
    }
    else 
    {
        label = (UILabel *)[cell.contentView viewWithTag:TABLE_LABEL_TAG];
        checkbox = (UIButton *)[cell.contentView viewWithTag:TABLE_CHECKBOX_TAG];
        
    }
    
    NSString *productStoreName = ((Store *)[storesArray objectAtIndex:indexPath.row]).storeName;
    NSString *productStoreAddress = ((Store *)[storesArray objectAtIndex:indexPath.row]).storeAddress;
    NSString *productStorePrice = ((Store *)[storesArray objectAtIndex:indexPath.row]).storeProductPrice;
    // BOOL isChecked = (BOOL)((Store *)[storesArray objectAtIndex:indexPath.row]).isStoreChecked;
    
    
    
    if(selectedRow == indexPath.row)
    {
        checkbox.selected = YES;
    }
    else {
        checkbox.selected = NO;
    }
    
    NSLog(@"storeName:%@",productStoreName);
    
    
    NSString *labelText =[ NSString stringWithFormat:@"Store-%d %@, %@,%@",indexPath.row+1,productStoreName,productStoreAddress,productStorePrice];
    label.text = labelText;
    
    
    return cell;
    
}
-(void)checkBoxClicked:(id)sender;
{
    UITableViewCell *selectedCell = (UITableViewCell *)[[sender superview]superview];
    selectedRow = [storesTable indexPathForCell:selectedCell].row;
    NSLog(@"Selected Row=%d",selectedRow);
    
    [storesTable reloadData ];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
