//
//  ProductListViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 18/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailViewController.h"


//============================================================================
//                    getStoreDetail parameters
//============================================================================
#define     STORE_ID                    @"store_id"    
#define     STORE_NAME                  @"store_name"  
#define     STORE_ADDRESS               @"store_address" 
#define     STORE_IMAGE_URL             @"store_logo"
#define     STORE_LATITUDE              @ "store_latitude"
#define     STORE_LONGITUDE             @ "store_longitude"
#define     STORE_PHONE_NUMBER          @ "phone_number"
#define     STORE_EMAIL                 @ "store_email"
#define     STORE_DESCRIPTION           @ "store_description"
//============================================================================
#define     STORE_PRODUCTS              @"storeProducts"     // AN ARRAY
//============================================================================
#define     STORE_PRODUCT_ID            @ "product_id"       // FIELDS OF ARRAY OBJECT
#define     STORE_PRODUCT_IMAGE_URL     @"main_image"
#define     STORE_PRODUCT_NAME          @ "product_name"
#define     STORE_PRODUCT_DESCRIPTION   @ "product_description"
//============================================================================



@interface StoreDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSInteger storeId;
    NSString *storeName;
    NSString *navButtonTitle;
    
    NSArray *productsArray;
    NSError *glbNetworkError;
    
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UITableView *productsTableView;

}


@property(nonatomic,strong) NSString *storeName;
@property(nonatomic,strong) NSString *navButtonTitle;
@property NSInteger storeId;
@end
