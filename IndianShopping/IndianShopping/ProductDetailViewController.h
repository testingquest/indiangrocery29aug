//
//  ProductDetailViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 14/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) 
//========================================================================================
#define PRODUCT_IMAGE_URL               @"main_image"
#define PRODUCT_ISAVAILABLE             @"is_available"
#define PRODUCT_MANUFACTURER_NAME       @"manufacturer_name"
#define PRODUCT_PRICE                   @"price"
#define PRODUCT_DESCRIPTION             @"product_description"
#define PRODUCT_ID                      @"product_id"
#define PRODUCT_NAME                    @"product_name"
#define PRODUCT_PRICE_RANGE             @"product_price_range"
//========================================================================================
#define PRODUCT_AVAILABILITY            @"product_availability"
//========================================================================================
#define PRODUCT_STORE_PRICE             @"price"
#define PRODUCT_STORE_SAVING            @"product_saving"
#define PRODUCT_STORE_ADDRESS           @"store_address"
#define PRODUCT_STORE_ID                @"store_id"
#define PRODCUT_STORE_LATITUDE          @"store_latitude"
#define PRODUCT_STORE_LONGITUDE         @"store_longitude"
#define PRODUCT_STORE_NAME              @"store_name"

// must have productName,quanitty,price, and saving       quantity is missing
//========================================================================================



@interface ProductDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSInteger productId;
    NSString *navButtonTitle;
    
    NSError *glbNetworkError;
    NSDictionary *productDetailDictionary;
    NSMutableArray *storesArray;
    NSInteger selectedRow;
    
    IBOutlet UITableView *storesTable;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UILabel *productName;
    IBOutlet UILabel *brand;
    IBOutlet UILabel *model;
    IBOutlet UILabel *priceRange;
    IBOutlet UILabel *moreDetail;
    IBOutlet UIButton *addButton;
    
    IBOutlet UIImageView *productImage;
    
    NSMutableArray *shoppingListArray;
   
    
}


@property NSInteger productId;
@property(nonatomic,strong) NSString *navButtonTitle;
@end
