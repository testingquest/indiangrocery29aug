//
//  LocationViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationViewController : UIViewController
{
    IBOutlet UITextField *emailTextField;
    IBOutlet UITextField *userPasswordTextField;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UIView *beforeLoginView;
    IBOutlet UIView *afterLoginView;
    
    
    NSString *loginEmail;
    NSString *loginUserId;

    
    NSError *glbNetworkError;
    
    IBOutlet UIButton *resellerCheckBox;
    IBOutlet UIView *resellerView;
}

-(IBAction)loginButtonPressed:(id)sender;
-(IBAction)registerButtonPressed:(id)sender;
-(IBAction)returnKeyPressed:(id)sender;
-(IBAction)savedShoppingListPressed:(id)sender;
-(IBAction)logoutPressed:(id)sender;
-(IBAction)checkBoxClicked:(id)sender;
-(IBAction)myAccountPressed:(id)sender;

@end
