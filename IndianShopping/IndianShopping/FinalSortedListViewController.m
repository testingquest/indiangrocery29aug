//
//  FinalSortedListViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 25/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FinalSortedListViewController.h"
#import "SortedShoppingListCell.h"
#import "Store.h"
#import "MailComposerViewController.h"
#import  <QuartzCore/QuartzCore.h>
#import "SBJson.h"
#import "AppDelegate.h"
#import "Store.h"
#import <MapKit/MapKit.h>
#import "DisplayRouteViewController.h"
#import <MQMapKit/MQMapKit.h>
//-----------------------------saveMyList input paramters--------------------
#define    SSL_USER_ID     @"user_id"
#define    SSL_LIST_NAME     @"list_name"
#define    SSL_USER_LATITUDE @"user_latitude"
#define    SSL_USER_LONGITUDE  @"user_longitude"

#define    SSL_PRODUCT_LIST  @"product_list"             //an array
#define     SSL_PRODUCT_ID  @"product_id"
#define     SSL_STORE_ID  @"store_id"
#define     SSLQUANTITY @"quantity"
#define     SSL_PRICE @"price"
#define     SSL_SAVING @"saving"
//-----------------------------saveMyList output paramters--------------------
#define shoppingListID    @"shoppinglist_id"
//------------------------------------------------------------------------------------
#define TEXT_ALERT  3


@interface FinalSortedListViewController ()

@end

@implementation FinalSortedListViewController
@synthesize unsortedArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isListFinalisedKey"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceived:) name:@"StoreAddedNotification" object:nil];
        
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog(@"unsortedArray=%@",unsortedArray);
    [saveButton setEnabled:YES];
    
    [self getLocation];
    // if value preference is 0
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"isListFinalisedKey"])
        [self.navigationController popViewControllerAnimated:NO];
}
-(void)viewDidDisappear:(BOOL)animated
{
    // [self.navigationController popViewControllerAnimated:YES];
    [super viewDidDisappear:animated];
}
-(void) notificationReceived:(NSNotification *)notification
{
    // this notification will be receive after user has finalised the list and is trying to
    // add new item to the new list
    // this notification should be received only once
    
    // [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)getLocation
{
    
    CLController = [[CoreLocationController alloc] init];
    CLController.delegate = self;
    [CLController.locMgr setDistanceFilter:100];
    [CLController.locMgr startUpdatingLocation];
    
    
}

- (void)locationError:(NSError *)error
{
    if(error.code== kCLErrorDenied )
    {
        UIAlertView *noLocationAlert = [[UIAlertView alloc]initWithTitle:@"Enable GPS" message:@"1) Go to Settings \n 2) Core Locations\n3) Indian Grocery\n     4)Turn the switch on" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //  noLocationAlert.tag =  LOCATION_ALERT_TAG;
        [noLocationAlert show];
        
        
    }
    
}
- (void)locationUpdate:(CLLocation *)location
{
    [CLController.locMgr stopUpdatingLocation];
    
    latitude = location.coordinate.latitude;
    longitude = location.coordinate.longitude;
    
    CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:latitude  longitude:longitude];
    
    NSLog(@"%f lat  %f long",latitude,longitude);
    
    sortedShoppingListArray = unsortedArray;
    
    for(Store *store in sortedShoppingListArray)
    {
        NSLog(@"store Distance=%f",store.distance);
    }
    
    
    //flaot d = 100;
    NSLog(@"UnsortedArray=%@",sortedShoppingListArray);
    
    //  NSMutableArray *storesCoordinateArray = [[NSMutableArray alloc]initWithCapacity:[sortedShoppingListArray count]];
    for(Store *store in sortedShoppingListArray)
    {
        CLLocationDegrees storeLat =  [store.storeLatitude floatValue];
        CLLocationDegrees storeLong =  [store.storeLongitude floatValue];
        
        CLLocation *storeLocation = [[CLLocation alloc] initWithLatitude:storeLat  longitude:storeLong];
        //[storesCoordinateArray addObject:storeLocation];
        // CLLocationDistance meters = [storeLocation distanceFromLocation:currentLocation];
        // store.distance = meters;    
        
        MQRoute *route = [[MQRoute alloc]init];
        route.delegate = self;
        
        route.bestFitRoute = true;
        route.routeType = MQRouteTypeShortest;
        
        [route getRouteWithStartCoordinate:currentLocation.coordinate endCoordinate:storeLocation.coordinate];
        
        [routesArray addObject:route];
        
        
    }
    
    
    
    
    
    //[myTable reloadData];
    /*dispatch_async(kBgQueue, ^{
     
     //from user defaults
     NSString *withInDistance = [[NSUserDefaults standardUserDefaults]valueForKey:@"distance"];
     
     NSLog(@"Product List Webservice   latitude=%f  longitude=%f  distance=%@ productPrefix=%@ ",latitude,longitude,withInDistance,productPrefix);
     
     
     
     // urlStringWithParameters = [NSString stringWithFormat:@"%@?withInDistance=%@&fromLatitude=%f&fromLongitude=%f&barcode=%@"          //NSLog(@"url:%@",urlStringWithParameters);
     
     
     NSError *error;
     NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:  @"http://maps.googleapis.com/maps/api/directions/json?origin=29.99,77.77&destination=29.55,77.55&waypoints=Joplin,MO%7COklahoma+City,OK&sensor=false"] options:0 error:&error];
     glbNetworkError = error;
     [self performSelectorOnMainThread:@selector(fetchedProductListData:) 
     withObject:data waitUntilDone:YES];
     });
     
     */
    
    
    //[self callDefaultServicesAsynchronously];
    //[self callDefaultServicesAsynchronously];
}


-(void)routeLoadFinished
{
    // get the raw xml passed back from the server:
    //NSLog(@"%@", route.rawXML);
    BOOL finished = NO;
    
    
    routeLoadCount = routeLoadCount+1;
    NSLog(@"RouteLoadCount=%d  and routeArrayCount=%d",routeLoadCount,[routesArray count]);
    if(routeLoadCount == [routesArray count])
    {
        finished = YES;
    }
    if(finished)
    {
        
        for(Store *store in sortedShoppingListArray)
        {
            int i=0;
            float distance = 0;
            MQRoute *route = (MQRoute*)[routesArray objectAtIndex:i];
            i = i+1;
            // do something with all the maneuvers
            for ( MQManeuver *maneuver in route.maneuvers )
            {
                NSLog(@"%@", maneuver.narrative);
                NSLog(@"Maneuver Distance=%f",maneuver.distance *1000);
                distance = distance + (maneuver.distance)*1000;
            }
            NSLog(@"Distance=%f",distance);  
            store.distance = distance;          // distance of each store from current location
        }
        
        NSSortDescriptor *aSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        //[sortedShoppingListArray sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
        NSArray *sortDescriptors = [NSArray arrayWithObject:aSortDescriptor];
        
        sortedShoppingListArray = [sortedShoppingListArray sortedArrayUsingDescriptors:sortDescriptors];
        NSLog(@"sortedArray=%@",sortedShoppingListArray);
        for(Store *store in sortedShoppingListArray)
        {
            NSLog(@"store Distance=%f",store.distance);
        }
        
        [myTable reloadData];   // list will be sorted on distance base now
        [activityIndicator stopAnimating];
    }
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    routeLoadCount = 0;
    routesArray = [[NSMutableArray alloc]initWithCapacity:[sortedShoppingListArray count]];
    // Do any additional setup after loading the view from its nib.
    
    
    [self getLocation];
    
    
    self.navigationItem.title = @"Final List";
    self.navigationItem.hidesBackButton = YES;
}


# pragma mark - table View methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [sortedShoppingListArray count];      
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    
    static NSString *identifier = @"CellIdentifier";
    
    SortedShoppingListCell *cell = (SortedShoppingListCell *)[tableView 
                                                              dequeueReusableCellWithIdentifier: identifier];
    if (cell == nil)  
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SortedShoppingListCell" owner:self options:nil];
        
        for (id oneObject in nib)
            if ([oneObject isKindOfClass:[SortedShoppingListCell class]])
                cell = (SortedShoppingListCell *)oneObject;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }    
    
    // NSLog(@"recent search string=%@",[recentSearchArray objectAtIndex:0]);
    
    if(indexPath.row %2 == 0)
    {
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    else {
        cell.contentView.backgroundColor = [UIColor lightGrayColor];
    }
    
    
    NSString *price = [NSString stringWithFormat:@"$%@",((Store*)[ sortedShoppingListArray objectAtIndex:indexPath.row]).storeProductPrice];
    
    NSInteger quantity = ((Store*)[ sortedShoppingListArray objectAtIndex:indexPath.row]).storeProductQuantity;
    NSInteger totalSavingForEntry = ([[NSString stringWithFormat:@"%@%",((Store*)[ sortedShoppingListArray objectAtIndex:indexPath.row]).storeProductSaving] floatValue] *quantity);
    
    
    cell.store.text =((Store*)[ sortedShoppingListArray objectAtIndex:indexPath.row]).storeName;
    cell.product.text = ((Store*)[ sortedShoppingListArray objectAtIndex:indexPath.row]).storeProductName;
    
    cell.quantity.text = [NSString stringWithFormat:@"%d",quantity];
    cell.price.text = price;
    cell.saving.text = [NSString stringWithFormat:@"%f",totalSavingForEntry];  
    
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - send email


-(IBAction)showMailPanel {
    
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            [self displayComposerSheet];
        }
        else
        {
            [self launchMailAppOnDevice];
        }
    }
    
    else
    {
        [self launchMailAppOnDevice];
    }
    
    
}

#pragma mark -
#pragma mark Compose Mail

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:@"Shopping List"];
    
    
    // Set up recipients
    // NSArray *toRecipients = [NSArray arrayWithObject:@"deep.arora@jagsonics.com"]; 
    // NSArray *ccRecipients = [NSArray arrayWithObjects:@"second@example.com", @"third@example.com", nil]; 
    // NSArray *bccRecipients = [NSArray arrayWithObject:@"fourth@example.com"]; 
    
    //[picker setToRecipients:toRecipients];
    // [picker setCcRecipients:ccRecipients];  
    // [picker setBccRecipients:bccRecipients];
    
    // Attach an image to the email
    /*  NSString *path = [[NSBundle mainBundle] pathForResource:@"rainy" ofType:@"png"];
     NSData *myData = [NSData dataWithContentsOfFile:path];
     [picker addAttachmentData:myData mimeType:@"image/png" fileName:@"rainy"];
     */
    // Fill out the email body text
    NSMutableString *body = (NSMutableString *)@"<table  border=\"1\"><tr style=\"font-weight:bold\"><th><font size = 1>Store</font></th><th><font size = 1>Product</font></th><th><font size = 1>Quantity</font></th><th><font size = 1>Price</font></th><th><font size = 1>Saving</font></th></tr>\n";
    
    for (int row = 0; row < [sortedShoppingListArray count]; row++) {
        Store * store = [sortedShoppingListArray objectAtIndex:row];
        
        // NSLog(@"storeName=%@ %@ ",store.storeName);
        
        body = (NSMutableString *)[body stringByAppendingFormat:[NSString stringWithFormat:@"<tr><td align=center>%@</td><td align=center>%@</td><td align=center>%d</td><td align=center>%@</td><td align=center>%@</td></tr>", store.storeName, store.storeProductName, store.storeProductQuantity, store.storeProductPrice,store.storeProductSaving]];
        
    }
    body =(NSMutableString *) [body stringByAppendingString:@"</font></table>"];
    [picker setMessageBody:body isHTML:YES];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [self presentModalViewController:picker animated:YES];
    
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{   
    NSLog(@"Result=%d",result);
    NSLog(@"Error=%@",error);
    NSString *message;
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = @"Mail canceled";
            break;
        case MFMailComposeResultSaved:
            message  = @"Mail saved";
            break;
        case MFMailComposeResultSent:
            message  = @"Mail sent";
            break;
        case MFMailComposeResultFailed:
            message = @"Delivery Failure";
            break;
        default:
            message  = @"Result: not sent";
            break;
    }
    [alertView setMessage:message];
    [alertView show];
    [self dismissModalViewControllerAnimated:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillShowNotification 
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];
}


#pragma mark -
#pragma mark Workaround

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
    /*  NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=Hello from California!";
     NSString *body = @"&body=It is raining in sunny California!";
     
     NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
     email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
     
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
     */
}




- (void)keyboardWillShow:(NSNotification *)note {
    //Get view that's controlling the keyboard
    UIWindow* keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIView* firstResponder = [keyWindow performSelector:@selector(firstResponder)];
    
    
    view = [[UIView alloc]initWithFrame:CGRectMake(-320, 155, 320, 45)];
    view.backgroundColor = [UIColor lightGrayColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(240,5,70,35);
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    
    button.backgroundColor = [UIColor darkGrayColor];
    button.layer.cornerRadius = 5;
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.layer.borderWidth = 1;
    
    
    
    [button setTitle:@"Done" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(dismissKeyboard:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [view addSubview:button];
    
    
    view.alpha = 0.0;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];  
    view.alpha = .9;
    view.frame = CGRectMake(0,155 ,320,45);
    [[[[[firstResponder superview] superview] superview] superview] addSubview:view];
    
    [UIView commitAnimations];
}

-(void)dismissKeyboard:(id)sender
{
    UIWindow* keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIView* firstResponder = [keyWindow performSelector:@selector(firstResponder)];
    [firstResponder resignFirstResponder];
    
}


- (void)keyboardWillHide:(NSNotification *)note {
    //hide button here
    [view removeFromSuperview];
    
}

#pragma mark - IBActions

-(IBAction)mapButtonClicked:(id)sender
{
    
    // add my current location to list
    NSMutableArray *uniqueStoresLocations = [[NSMutableArray alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    [uniqueStoresLocations addObject:loc];    // add my location at zero index
    
    
    // fetching unique stores
    NSMutableArray *uniqueStoreArray = [[NSMutableArray alloc]init ];
    for( int i =0 ; i < [sortedShoppingListArray count]; i++)
    {
        Store *store = [sortedShoppingListArray objectAtIndex:i];   //
        
        
        NSPredicate * myLittlePredicate = [NSPredicate predicateWithFormat:@"(storeName like %@ )", store.storeName];
        NSArray * result = [uniqueStoreArray filteredArrayUsingPredicate:myLittlePredicate];
        
        if(result && [result count] > 0)
        {
            // do not add in array
        }
        else
        {
            
            [uniqueStoreArray addObject:store];                
            
        }
        
    }
    
    NSLog(@"the value in array %@",uniqueStoreArray);
    //creating location array for unique stores
    for(Store *store in uniqueStoreArray)
    {
        CLLocationDegrees strLatitude =  [store.storeLatitude floatValue];
        CLLocationDegrees strLongitude =   [store.storeLongitude floatValue];
        
        CLLocation *loc = [[CLLocation alloc]initWithLatitude:strLatitude longitude:strLongitude];
        [uniqueStoresLocations addObject:loc];
        
                
    }
    
    [uniqueStoresLocations addObject:[uniqueStoresLocations objectAtIndex:0]];  //add my loc to last index
    DisplayRouteViewController *cntrl = [[DisplayRouteViewController alloc]initWithNibName:nil bundle:nil];
    cntrl.points = uniqueStoresLocations;
    [self.navigationController pushViewController:cntrl animated:YES];
    
    
}
#pragma mark - alert

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 0)
    {
        if(alertView.tag == TEXT_ALERT)
        {
            listName = alertTextField.text;
            if((listName.length == 0) || (listName == nil))
            {
                listNameLabel.text = @"No Name";
            }
            else
            {
                listNameLabel.text = listName;
            }
            dispatch_async(kBgQueue, ^{
                
                int count = [sortedShoppingListArray count];
                
                NSMutableArray *productList = [[NSMutableArray alloc]initWithCapacity:count];
                for(int i=0;i<count;i++)
                {
                    NSDictionary *productDict = [[NSMutableDictionary alloc]init];
                    
                    NSString *productID = [NSString stringWithFormat:@"%d",((Store*)[sortedShoppingListArray objectAtIndex:i]).storeProductId];
                    
                    NSString *storeID = [NSString stringWithFormat:@"%d",((Store*)[sortedShoppingListArray objectAtIndex:i]).storeId];
                    
                    NSString *quantity = [NSString stringWithFormat:@"%d",((Store*)[sortedShoppingListArray objectAtIndex:i]).storeProductQuantity];
                    
                    NSString *price = ((Store*)[sortedShoppingListArray objectAtIndex:i]).storeProductPrice;
                    
                    NSString *saving =  ((Store*)[sortedShoppingListArray objectAtIndex:i]).storeProductSaving;
                    
                    [productDict setValue:productID forKey:SSL_PRODUCT_ID];
                    [productDict setValue:storeID forKey:SSL_STORE_ID];
                    [productDict setValue:quantity forKey:SSLQUANTITY];
                    [productDict setValue:price forKey:SSL_PRICE];
                    [productDict setValue:saving forKey:SSL_SAVING];
                    
                    
                    [productList addObject:productDict];
                }
                
                NSString *productListJSON = [productList JSONRepresentation];
                NSLog(@"productListJson=%@",productListJSON);
                
                NSString *str = [productListJSON stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                
                
                NSString * user_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"userId"];
                NSLog(@"userID=%@",user_id);
                NSString *list_name=listName;
                NSLog(@"%f latitude %f longitude listName=%@",latitude,longitude,listName);
                
                
                
                NSString *urlStringWithParameters = [NSString stringWithFormat:@"%@?user_id=%@&list_name=%@&user_latitude=%f&user_longitude=%f&product_list=%@",saveMyListUrl,user_id,list_name,latitude,longitude,str];
                
                NSLog(@"url:%@",urlStringWithParameters);
                
                NSError *error;
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: urlStringWithParameters ]options:0 error:&error];
                //  NSLog(@"Error=%@,error");
                glbNetworkError = error;
                [self performSelectorOnMainThread:@selector(saveListResponseData:) 
                                       withObject:data waitUntilDone:YES];
                
            });
            

            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"reSellerId"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            //user wants to login, redirect him
            AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            [delegate setIsSaveListTapped:YES];
            
            self.tabBarController.selectedIndex = 3; 
        }
        
        
        
    }
    
    
}

-(void)textAlertView
{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Enter list name \n \n" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [alertView setFrame:CGRectMake(20, 100, 280, 250)];
    alertView.tag = TEXT_ALERT;
    
    alertTextField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 30.0)];
    [alertTextField setBackgroundColor:[UIColor whiteColor]];
    alertTextField.placeholder = @"List Name";
    alertTextField.borderStyle = UITextBorderStyleRoundedRect;
    alertTextField.textAlignment = UITextAlignmentCenter;
    alertTextField.font = [UIFont systemFontOfSize:14];
    alertTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    


    [alertView addSubview:alertTextField];
    
    [alertView show];
    
}

-(IBAction)saveShoppingList:(id)sender
{
    [saveButton setEnabled:NO];
    NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:@"userId"];
   NSString *resellerId = [[NSUserDefaults standardUserDefaults]valueForKey:@"reSellerId"];
    if((userId == nil) && (resellerId == nil))
    {
        // user not logged in, redirect him to login page
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Not Logged in" message:@"Login as a buyer to save the list" delegate:self cancelButtonTitle:@"Login" otherButtonTitles:@"Dont Save", nil];
        [alert show];
        [saveButton setEnabled:YES];
    }
    else if((userId == nil) && (resellerId != nil))
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Logged in as a reseller" message:@"Do you want to Login as a buyer inorder to save the list" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        [alert show];
        [saveButton setEnabled:YES];  
    }
        
    else {
        [activityIndicator startAnimating];
        [self textAlertView];    // show the alert view
               
    }
}

- (void)saveListResponseData:(NSData *)responseData {
    
    
    [activityIndicator stopAnimating];
    //parse out the json data
    
    if (glbNetworkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
        [saveButton setEnabled:YES];
    }
    else {
        
        
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *json_string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSDictionary *json = [parser objectWithString:json_string error:&error];
        
        NSLog(@"Save list response:%@",json);
        if (error == nil)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"List successfully saved" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles: nil  ];
            [alert show];
            
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"shoppingListKey"];  
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isListFinalisedKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else {
            
            NSLog(@"Parsing problem");
            
        }
        
        
        
    }
    
}




- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
