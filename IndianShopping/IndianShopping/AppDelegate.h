//
//  AppDelegate.h
//  IndianShopping
//
//  Created by Jagsonics on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreLocation/CoreLocation.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>
{
     UINavigationController *navigationControllerHome,*navigationControllerSearch,*navigationControllerShoppingCart, *navigationControllerLocation,*navigationControllerDisclaimer;
    
    
    CLLocationManager *locationManager;
    NSString *latitude;
    NSString *longitude;
   BOOL isSaveListTapped;
}
 


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property BOOL isSaveListTapped;





- (UIColor *) colorWithHexString: (NSString *) hex  ;
@end
