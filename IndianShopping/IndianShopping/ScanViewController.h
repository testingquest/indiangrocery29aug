//
//  ScanViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"

@interface ScanViewController : UIViewController<ZBarReaderDelegate>

{
    IBOutlet UIImageView *resultImage;
    IBOutlet UITextView *resultText;
  
    NSMutableString *scannedText;
}


- (IBAction) scanButtonTapped;

@end



